from django.db import models

class Area(models.Model):
    nombre = models.CharField(max_length=100)
    sigla = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre


class Cargo(models.Model):
    nombre = models.CharField(max_length=100)
    sigla = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre


class Usuario(models.Model):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    usuario = models.CharField(max_length=50)
    estado = models.BooleanField(default=True)
    documento = models.CharField(max_length=50)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE)
    telefono = models.IntegerField()

    def __str__(self):
        return self.nombres + ' ' + self.apellidos


class Marcado(models.Model):
    usuario = models.CharField(max_length=50)
    hora = models.TimeField(default=None, blank=True, null=True)
    fecha = models.DateField(default=None, blank=True, null=True)
    periodo = models.CharField(max_length=10)

    def __str__(self):
        return self.usuario

