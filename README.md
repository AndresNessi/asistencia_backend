# Backend Registro Biometrico

> Este proyecto es el servicio de backend del sistema de registro biometrico del personal.

## Build Setup

``` bash
# Install dependenciesI
pip3 install -r requeriments.txt

# Armar la base de datos
python3 manage.py makemigrations

# Migrar la base de datos
python3 manage.py migrate

# Crear el superusuario para el panel de administrador
python3 manage.py createsuperuser

# Arrancar el proyecto
python3 manage.py runserver 0.0.0.0:8000

```
