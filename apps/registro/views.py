from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from django.http import Http404
from .models import Area, Cargo, Usuario, Marcado
from .serializers import AreaSerializer, CargoSerializer, UsuarioSerializer, MarcadoSerializer, PersonalSerializer, Usuario2Serializer

# LISTA DE LAS AREAS
class AreaList(APIView):

    def get(self, request, format=None):
        area = Area.objects.all()
        serializer = AreaSerializer(area, many=True)
        return Response(serializer.data)


# LISTA DE LAS CARGOS
class CargoList(APIView):

    def get(self, request, format=None):
        cargo = Cargo.objects.all()
        serializer = CargoSerializer(cargo, many=True)
        return Response(serializer.data)


# LISTA DE LAS USUARIOS
class UsuarioList(APIView):

    def get(self, request, format=None):
        usuario = Usuario.objects.all()
        serializer = UsuarioSerializer(usuario, many=True)
        return Response(serializer.data)


# LISTA DE LAS USUARIOS
class PersonalList(APIView):

    def get(self, request, format=None):
        usuario = Usuario.objects.all()
        serializer = PersonalSerializer(usuario, many=True)
        return Response(serializer.data)


# LISTA DE LOS MARCADOS
class MarcadoList(APIView):

    def get(self, request, format=None):
        marcado = Marcado.objects.all()
        serializer = MarcadoSerializer(marcado, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = MarcadoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# LISTA NUEVOS USUARIOS
class Usuarios2List(APIView):

    def get(self, request, format=None):
        usuario = Usuario.objects.all()
        serializer = Usuario2Serializer(usuario, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = Usuario2Serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Usuario2Detail(APIView):

    def get_object(self, pk):
        try:
            return Usuario.objects.get(pk=pk)
        except Snippet.DoesNotExist:
            raise Http404

    def get(self, request, pk,format=None):
        snippet = self.get_object(pk)
        serializer = Usuario2Serializer(snippet)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = Usuario2Serializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request,pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class MarcadosList(APIView):

    def get(self, request, pk1, pk2, pk3):
        snippets = Marcado.objects.filter(usuario=pk1, fecha__gt=pk2, fecha__lt=pk3).order_by('-fecha')
        serializer = MarcadoSerializer(snippets, many=True)
        return Response(serializer.data)
