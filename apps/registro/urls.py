from django.urls import path, include
from .views import AreaList, MarcadoList, UsuarioList, PersonalList, Usuarios2List, CargoList,Usuario2Detail, MarcadosList

urlpatterns = [
    # ////////////////////////////////////////
    # URLS DE LA PARTE DEL CLIENTE
    # ////////////////////////////////////////
    path('api/areas/', AreaList.as_view()),
    path('api/cargos/', CargoList.as_view()),
    path('api/marcados/', MarcadoList.as_view()),
    path('api/usuarios/', UsuarioList.as_view()),
    path('api/personal/', PersonalList.as_view()),
    path('api/usuarios2/', Usuarios2List.as_view()),
    path('api/usuarios2/<int:pk>/', Usuario2Detail.as_view()),
    path('api/marcados/<str:pk1>/<str:pk2>/<str:pk3>/', MarcadosList.as_view()),
]
