from django.contrib import admin
from .models import Area, Cargo, Usuario, Marcado
# Register your models here.
admin.site.register(Usuario)
admin.site.register(Area)
admin.site.register(Cargo)
admin.site.register(Marcado)
