from rest_framework import serializers
from .models import Cargo, Area, Usuario, Marcado


class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = '__all__'


class CargoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cargo
        fields = '__all__'


class MarcadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marcado
        fields = '__all__'


class UsuarioSerializer(serializers.ModelSerializer):
    area = AreaSerializer(read_only=True)
    cargo = CargoSerializer(read_only=True)

    class Meta:
        model = Usuario
        fields = [
            'id',
            'nombres',
            'apellidos',
            'usuario',
            'estado',
            'documento',
            'area',
            'cargo',
            'telefono'
        ]


class Usuario2Serializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = '__all__'


class PersonalSerializer(serializers.ModelSerializer):

    class Meta:
        model = Usuario
        fields = [
            'usuario'
        ]
